<?php

namespace App\Actions\Authentication;

use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Lorisleiva\Actions\Concerns\AsAction;

class LoginUser
{
    use AsAction;

    public function handle(String $email, String $password)
    {
        return Auth::attempt([
            "email" => $email,
            "password" => $password
        ]);
    }

    public function rules()
    {
        return [
            "email" => ["required", "email"],
            "password" => ["required", "string"]
        ];
    }

    public function asController(Request $request)
    {
        $token = $this->handle($request->get("email"), $request->get("password"));
        if (!$token) {
            return response()->json([
                "message" => "invalid login"
            ], 401);
        }
        return response()->json([
            "token" => $token
        ]);
    }
}

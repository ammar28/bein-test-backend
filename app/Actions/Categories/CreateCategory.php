<?php

namespace App\Actions\Categories;

use App\Models\Category;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Lorisleiva\Actions\Concerns\AsAction;
use Illuminate\Validation\Validator;
use Lorisleiva\Actions\ActionRequest;

class CreateCategory
{
    use AsAction;

    public function handle($name, $discount = 0, $parent_id = null)
    {
        $parent_category = null;
        if ($parent_id) {
            $parent_category = Category::find($parent_id);
        }
        $category = new Category();
        $category->menu_id = Auth::user()->menu->id;
        $category->name = $name;
        if ($discount) {
            $category->discount = $discount;
            $category->discount_inherited = false;
        } else if ($parent_category) {
            $category->discount = $parent_category->discount;
            $category->discount_inherited = true;
        }
        if (!$parent_category) {
            $category->saveAsRoot();
        } else {
            $category->appendToNode($parent_category);
        }
        $category->save();
    }

    public function rules()
    {
        return [
            "name" => ["required", "string"],
            "discount" => ["int", "between:0,99"],
            "parent_id" => ["exists:categories,id", "nullable"]
        ];
    }

    public function afterValidator(Validator $validator, ActionRequest $request): void
    {
        if ($request->has("parent_id") && $request->get("parent_id")) {
            $category = Category::withDepth()->find($request->get("parent_id"));
            if ($category->depth >= 3) {
                $validator->errors()->add('parent_id', 'Max Nested Categories is 4');
            }
        }
    }


    public function asController(Request $request)
    {
        $this->handle(
            $request->get("name"),
            $request->get("discount"),
            $request->get("parent_id")
        );
    }
}

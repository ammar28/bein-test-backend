<?php

namespace App\Actions\Categories;

use App\Models\Category;
use App\Models\Item;
use Lorisleiva\Actions\Concerns\AsAction;
use Illuminate\Http\Request;

class UpdateCategory
{
    use AsAction;

    public function handle(Category $category, string $name, int $discount)
    {
        $category->name = $name;
        $category->discount = $discount;
        $category->discount_inherited = false;
        $category->save();
        // update discounts on child categories where discount is inherited
        $category->descendants()->where("discount_inherited", true)->update(
            ["discount" => $discount]
        );
        // update discounts on all items in category or subcategories where discount is inherited
        $categories = $category->descendants()->pluck('id');
        $categories[] = $category->getKey();
        Item::query()->whereIn('category_id', $categories)->where("discount_inherited", true)->update(["discount" => $discount]);
    }

    public function rules()
    {
        return [
            "name" => ["required", "string"],
            "discount" => ["int", "between:0,99"]
        ];
    }

    public function asController(Request $request, Category $category)
    {
        $this->handle(
            $category,
            $request->get("name"),
            $request->get("discount")
        );
    }
}

<?php

namespace App\Actions\Items;

use App\Models\Category;
use App\Models\Item;
use Illuminate\Http\Request;
use Lorisleiva\Actions\Concerns\AsAction;

class CreateItem
{
    use AsAction;

    public function handle(string $name, int $category_id, int|null $discount)
    {
        $category = Category::find($category_id);
        $item = new Item();
        $item->name = $name;
        $item->category_id = $category_id;
        if ($discount) {
            $item->discount = $discount;
            $item->discount_inherited = false;
        } else {
            $item->discount = $category->discount;
            $item->discount_inherited = true;
        }
        $item->save();
    }

    public function rules()
    {
        return [
            "name" => ["required", "string"],
            "category_id" => ["required", "exists:categories,id"],
            "discount" => ["integer", "between:0,99"]
        ];
    }

    public function asController(Request $request)
    {
        $this->handle(
            $request->get("name"),
            $request->get("category_id"),
            $request->get("discount")
        );

        return response("", 201);
    }
}

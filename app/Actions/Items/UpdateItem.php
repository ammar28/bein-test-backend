<?php

namespace App\Actions\Items;

use Illuminate\Http\Request;
use Lorisleiva\Actions\Concerns\AsAction;
use App\Models\Item;

class UpdateItem
{
    use AsAction;

    public function handle(Item $item, string $name, int|null $discount)
    {
        $item->name = $name;
        if ($discount) {
            $item->discount = $discount;
            $item->discount_inherited = false;
        } else {
            $item->discount = $item->category->discount;
            $item->discount_inherited = true;
        }
        $item->save();
    }

    public function rules()
    {
        return [
            "name" => ["required", "string"],
            "discount" => ["integer", "between:0,99"]
        ];
    }

    public function asController(Request $request, Item $item)
    {
        $this->handle(
            $item,
            $request->get("name"),
            $request->get("discount")
        );

        return response("", 201);
    }
}

<?php

namespace App\Actions\Menus;

use App\Models\Menu;
use App\Models\User;
use Lorisleiva\Actions\Concerns\AsAction;

class CreateMenu
{
    use AsAction;

    public function handle($user_id)
    {
        $menu = new Menu();
        $menu->user_id = $user_id;
        $menu->save();
        return $menu;
    }
}

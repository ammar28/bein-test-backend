<?php

namespace App\Actions\Menus;

use Illuminate\Support\Facades\Auth;
use Lorisleiva\Actions\Concerns\AsAction;

class GetMenu
{
    use AsAction;

    public function handle()
    {
        $menu = Auth::user()->menu;
        return response()->json([
            "menu" => $menu,
            "categories" => $menu->categories()->withDepth()->with("items")->get()->toTree()
        ]);
    }
}

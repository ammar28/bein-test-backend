<?php

namespace App\Actions\Menus;

use App\Actions\Categories\UpdateCategory;
use App\Models\Category;
use App\Models\Item;
use App\Models\Menu;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Lorisleiva\Actions\Concerns\AsAction;

class UpdateMenu
{
    use AsAction;

    public function handle(Menu $menu, int | null $discount)
    {
        $menu->discount = $discount;
        $menu->save();
        // updating categories discount
        Category::where("menu_id", $menu->id)->update([
            "discount" => $discount,
            "discount_inherited" => true
        ]);
        // updating items discounts
        $categories_ids = Category::where("menu_id", $menu->id)->get()->pluck("id");
        Item::whereIn("category_id", $categories_ids)->update([
            "discount" => $discount,
            "discount_inherited" => true
        ]);
    }

    public function asController(Request $request)
    {
        $menu = Auth::user()->menu;
        $this->handle($menu, $request->get("discount"));
    }
}

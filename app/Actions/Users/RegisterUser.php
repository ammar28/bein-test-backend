<?php

namespace App\Actions\Users;

use App\Actions\Menus\CreateMenu;
use App\Models\User;
use Lorisleiva\Actions\Concerns\AsAction;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;

class RegisterUser
{
    use AsAction;

    public function rules()
    {
        return [
            'name' => ['required', 'string'],
            'email' => ['required', 'unique:users,email'],
            'password' => ['required', 'confirmed'],
        ];
    }

    public function handle(String $name, String $email, String $password)
    {
        $user = new User();
        $user->name = $name;
        $user->email = $email;
        $user->password = Hash::make($password);
        $user->save();

        CreateMenu::run($user->id);
    }

    public function asController(Request $request)
    {
        $this->handle(
            $request->get("name"),
            $request->get("email"),
            $request->get('password')
        );

        // return success 201 (created)
        return response("", 201);
    }
}

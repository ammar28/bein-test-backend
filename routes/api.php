<?php

use App\Actions\Authentication\LoginUser;
use App\Actions\Categories\CreateCategory;
use App\Actions\Categories\UpdateCategory;
use App\Actions\Items\CreateItem;
use App\Actions\Items\UpdateItem;
use App\Actions\Menus\GetMenu;
use App\Actions\Menus\UpdateMenu;
use App\Actions\Users\RegisterUser;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider and all of them will
| be assigned to the "api" middleware group. Make something great!
|
*/

Route::middleware('auth:sanctum')->get('/user', function (Request $request) {
    return $request->user();
});

Route::post('register', RegisterUser::class);
Route::post('login', LoginUser::class);

Route::group([
    "middleware" => ["auth"]
], function () {
    Route::post("categories", CreateCategory::class);
    Route::put("categories/{category}", UpdateCategory::class);
    Route::post("items", CreateItem::class);
    Route::put("items/{item}", UpdateItem::class);
    Route::get("menu", GetMenu::class);
    Route::put("menu", UpdateMenu::class);
});
